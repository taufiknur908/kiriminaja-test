module.exports = {
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}'
  ],
  theme: {
    fontFamily: {
      sans: ['Poppins', 'sans-serif']
    },
    extend: {
      colors: {
        dark: {
          primary: '#333',
          secondary: '#555',
          tertiary: '#777'
        },
        light: {
          primary: '#F6EDFF',
          secondary: '#eee',
          tertiary: '#ddd'
        },
        primary: {
          default: '#D6C4FD',
          50: '#f6f3ff',
          100: '#efe9fe',
          200: '#e1d6fe',
          300: '#d6c4fd',
          400: '#b38cf9',
          500: '#9b5df5',
          600: '#8f3bec',
          700: '#8029d8',
          800: '#6b22b5',
          900: '#591e94'
        }
      }
    }
  },
  plugins: [require('@tailwindcss/forms'), require('@tailwindcss/aspect-ratio')]
}

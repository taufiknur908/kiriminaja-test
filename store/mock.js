const state = () => ({
  allExpedisi: []
})

const getters = {
  allExpedisi: state => state.allExpedisi
}

const actions = {
  async getAllExpedisi ({ commit }, page) {
    const response = await this.$axios.$get('expedisi')
    await commit('getAllExpedisi', response)
  }
}

const mutations = {
  getAllExpedisi (state, payload) {
    state.allExpedisi = payload.data
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
